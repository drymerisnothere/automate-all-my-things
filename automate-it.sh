#!/usr/bin/env bash

set -ex
# set -o pipefail

####################
# Variables:       #
# REPO_NAME        #
# REPO_DESCRIPTION #
# CLONE_ADDRESS    #
# DRONE_TOKEN      #
# DRONE_HOST       #
# XMPP_ROOM        #
# XMPP_USER        #
# XMPP_PASSWORD    #
# CLONE_ADDRESS    #
# GITEA_URL        #
# GITEA_TOKEN      #
# DOCKER_PASSWORD  #
# DOCKER_HOST      #
# DOCKER_USERNAME  #
####################

GITEA_URL="${GITEA_URL:-https://git.daemons.it}"
DRONE_CLI_VERSION="0.8-alpine"

function create_normal_repository {
    echo "Creating drymer/$REPO_NAME repository..."
    curl --silent -X POST "$GITEA_URL/api/v1/user/repos" \
        -H "accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Authorization: token $GITEA_TOKEN" \
        -d "{\"auto_init\": true, \"license\": \"GPL-3.0-only\", \"readme\": \"Default\", \"name\": \"$REPO_NAME\", \"description\": \"$REPO_DESCRIPTION\"}"
}

function create_mirror_repository {
    echo "Creating drymer/$REPO_NAME mirror..."
    curl --silent -X POST "$GITEA_URL/api/v1/repos/migrate" \
        -H "accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Authorization: token $GITEA_TOKEN" \
        -d "{\"clone_addr\": \"$CLONE_ADDRESS\", \"description\": \"$REPO_DESCRIPTION\", \"mirror\": true, \"repo_name\": \"$REPO_NAME\", \"uid\": 1}"
}

function drone_set_xmpp_secrets {
    echo "Creating drymer/$REPO_NAME XMPP credentials..."
    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION secret add \
        --repository "drymer/$REPO_NAME" \
        --name xmpp_room \
        --value $XMPP_ROOM \
        --event push \
        --event pull_request
    sleep 1

    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION secret add \
        --repository "drymer/$REPO_NAME" \
        --name xmpp_password \
        --value $XMPP_PASSWORD \
        --event push \
        --event pull_request

    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION secret add \
        --repository "drymer/$REPO_NAME" \
        --name xmpp_user \
        --value $XMPP_USER \
        --event push \
        --event pull_request
}

function drone_set_registry_secrets {
    echo "Creating drymer/$REPO_NAME docker credentials..."
    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION repo update \
        --trusted "drymer/$REPO_NAME"

    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION secret add \
        --repository "drymer/$REPO_NAME" \
        --name docker_user \
        --value $DOCKER_USERNAME

    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION secret add \
        --repository "drymer/$REPO_NAME" \
        --name docker_password \
        --value $DOCKER_PASSWORD

    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION secret add \
        --repository "drymer/$REPO_NAME" \
        --name docker_registry \
        --value $DOCKER_REGISTRY
}

function drone_set_main {
    echo "Syncing drone for drymer/$REPO_NAME..."
    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION repo sync > /dev/null

    echo "Creating drymer/$REPO_NAME drone pipeline..."
    docker run -i \
        -e DRONE_SERVER=$DRONE_SERVER \
        -e DRONE_TOKEN=$DRONE_TOKEN \
        drone/cli:$DRONE_CLI_VERSION repo add "drymer/$REPO_NAME"
}

NEW_FILES="$(git diff-tree --no-commit-id --name-only -r HEAD | grep projects)"

for file in $NEW_FILES
do
    # shellcheck disable=SC1090
    . "$file"

    if [[ -n $MIRROR ]]
    then
        CLONE_ADDRESS=$MIRROR
        create_mirror_repository
    else
        create_normal_repository
    fi

    if [[ -n $DRONE || -n $DOCKER || -n $XMPP ]]
    then
        drone_set_main
    fi

    if [[ -n $XMPP ]]
    then
        drone_set_xmpp_secrets
    fi

    if [[ -n $DOCKER ]]
    then
        drone_set_registry_secrets
    fi

    unset REPO_NAME
    unset REPO_DESCRIPTION
    unset DOCKER
    unset MIRROR
done

set +ex
