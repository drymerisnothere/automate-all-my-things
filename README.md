# Automate All My things
Create git projects associated with drone and some credentials.

It's possible to use the next variables:
- **REPO_NAME**: Git repository.
- **REPO_DESCRIPTION**: Git repository description.
- **DOCKER**: Add user, password and registry's credentials.
- **XMPP**: Add xmpp user, password and MUC credentials.
